FROM php:8.0.21-zts-bullseye@sha256:c7fc366e2bac85a5e0d26d1cb7d7770d30d33d4c0d4c6b931c9b409005b819ad AS runtime

RUN apt-get update -y && apt-get upgrade -y

RUN apt install php7.4-curl

ADD php.ini /usr/local/etc/php

WORKDIR /src

ADD . .

WORKDIR /src/public

CMD ["php", "-S", "0.0.0.0:80"]