<?php

namespace JsonRpc;

class RpcApiTools
{
    public static function encode($value)
    {
        return json_encode($value);
    }

    public static function decode($msg)
    {
        return json_decode($msg);
    }
}