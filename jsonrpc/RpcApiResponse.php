<?php

namespace JsonRpc;

class RpcApiResponse
{
    protected $jsonrpc;
    protected $result;
    protected $error;
    protected $id;

    public function __construct($jsonrpc, $result, $error, $id)
    {
        $this->jsonrpc = $jsonrpc;
        $this->result = $result;
        $this->error = $error;
        $this->id = $id;
    }

    public function __toString()
    {
        return RpcApiTools::encode(array(
            "jsonrpc" => $this->jsonrpc,
            "result" => $this->result,
            "error" => $this->error,
            "id" => $this->id
        ));
    }

}