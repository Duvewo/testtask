<?php

namespace JsonRpc;

class RpcApi
{
    protected $jsonrpc;
    protected $id;
    protected $method;
    protected $params;

    public function __construct($version, $id, $method, $params)
    {
        $this->jsonrpc = $version;
        $this->id = $id;
        $this->method = $method;
        $this->params = $params;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getJsonrpc()
    {
        return $this->jsonrpc;
    }

    /**
     * @return mixed
     */
    public function getMethod()
    {
        return $this->method;
    }

    /**
     * @return mixed
     */
    public function getParams()
    {
        return $this->params;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @param mixed $jsonrpc
     */
    public function setJsonrpc($jsonrpc)
    {
        $this->jsonrpc = $jsonrpc;
    }

    /**
     * @param mixed $method
     */
    public function setMethod($method)
    {
        $this->method = $method;
    }

    /**
     * @param mixed $params
     */
    public function setParams($params)
    {
        $this->params = $params;
    }

    public function __toString()
    {
        return RpcApiTools::encode(array(
            "jsonrpc" => $this->jsonrpc,
            "id" => $this->id,
            "method" => $this->method,
            "params" => $this->params
        ));
    }


}
