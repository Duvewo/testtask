<?php

class TimeServiceRpc
{
    public static function getDateTime()
    {
        return DB::getCurrentTime();
    }

    public static function getUnixTime()
    {
        return floor(microtime(true) * 1000);
    }

}