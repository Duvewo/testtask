<?php

namespace JsonRpc;

class RpcErrorDefinition
{
    private $code;
    private $message;
    private $data;

    public function __construct($code, $message, $data = null)
    {
        $this->code = $code;
        $this->message = $message;
        $this->data = $data;
    }

    public function __toString()
    {
        return RpcApiTools::encode(array(
            "code" => $this->code,
            "message" => $this->message,
            "data" => $this->data
        ));
    }

}