<?php

use JsonRpc\RpcApi;
use JsonRpc\RpcApiTools;
use JsonRpc\RpcErrorDefinition;
use JsonRpc\RpcApiResponse;

if ($_SERVER["REQUEST_METHOD"] != "POST") {
    die("POST only!");
}

$body = RpcApiTools::decode(file_get_contents("php://input"));

$rpc = new RpcApi($body->jsonrpc, $body->id, $body->method, $body->params);

if ($body->jsonrpc != "2.0") {
    echo new RpcApiResponse("2.0", $rpc->getId(), RpcApiTools::encode((string)new RpcErrorDefinition(3, "Unsupported version")), $rpc->getParams());
    return;
}

switch ($rpc->getMethod()) {
    case "getTimeRpc":
        if ($rpc->getParams() == "datetime") {
            echo TimeServiceRpc::getDateTime();
        } else if ($rpc->getParams() == "unixtime") {
            echo TimeServiceRpc::getUnixTime();
        } else {
            echo new RpcApiResponse($rpc->getJsonrpc(), null, RpcApiTools::encode((string)new RpcErrorDefinition(1, "Invalid parameter provided")), $rpc->getId());
        }
        break;
    default:
        echo new RpcApiResponse($rpc->getJsonrpc(), null, RpcApiTools::encode((string)new RpcErrorDefinition(2, "Invalid method provided")), $rpc->getId());
        break;
}