<?php
/*
 * A configuration file to set a default connection and multiple database connections.
 * This file must be hidden when copying the project, as it contains important information.
 *
 * Конфигурационный файл для задания подключения по умолчанию и нескольких вариантов подключений к базе данных.
 * Этот файл необходимо скрывать при копировании проекта, так как он содержит важную информацию.
 */

const HLEB_TYPE_DB = 'mysql.name';

const HLEB_PARAMETERS_FOR_DB = [

    'mysql.name' => [
        'mysql:host=localhost',
        'port=3306',
        'dbname=example',
        'charset=utf8',
        'user' => 'root',
        'pass' => 'example'
    ],
];

